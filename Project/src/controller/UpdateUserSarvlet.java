package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UpdateUserSarvlet
 */
@WebServlet("/UpdateUserSarvlet")
public class UpdateUserSarvlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateUserSarvlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");

		UserDao userDeta = new UserDao();
		User upuserdeta= userDeta.findByUpuserDateInfo(id);

		request.setAttribute("userDate", upuserdeta);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update_user.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");

		String Name = request.getParameter("name");
		String birthDate = request.getParameter("birthdate");
		String password1 = request.getParameter("pass1");
		String password2 = request.getParameter("pass2");
		String loginId = request.getParameter("id");


		if(!password1.equals(password2)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/update_user.jsp");
			dispatcher1.forward(request, response);
			return;
		}

		if(	Name.equals("") || birthDate.equals("")) {

			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/update_user.jsp");
			dispatcher1.forward(request, response);
			return;
		}

			UserDao userDao = new UserDao();

			if(password1.equals("") || password2.equals("")) {
				int upuserDate= userDao.findByUpdateInfo(Name,birthDate,loginId);
			}else {
			String signal = userDao.findBySignaluserInfo(password1);
			int upuser= userDao.findByUpdateUserInfo(Name,birthDate,signal,password2,loginId);
			}




		response.sendRedirect("UserListServlet");

	}

}

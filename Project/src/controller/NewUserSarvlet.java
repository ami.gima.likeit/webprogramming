package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;


/**
 * Servlet implementation class NewUserSarvlet
 */
@WebServlet("/NewUserSarvlet")
public class NewUserSarvlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewUserSarvlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new_user.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("id");
		String password1 = request.getParameter("pass1");
		String password2 = request.getParameter("pass2");
		String Name = request.getParameter("name");
		String birthDate = request.getParameter("birthdate");

		UserDao userDao = new UserDao();
		User newuser = userDao.findBynewUserInfo(loginId);


		if(newuser != null) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/new_user.jsp");
			dispatcher1.forward(request, response);
			return;
		}

		if(!password1.equals(password2)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/new_user.jsp");
			dispatcher1.forward(request, response);
			return;
		}
			if(loginId.equals("") || password1.equals("") || password2.equals("") || Name.equals("") || birthDate.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");

				RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/new_user.jsp");
				dispatcher1.forward(request, response);
				return;
			}


			String signal = userDao.findBySignaluserInfo(password1);
		int i = userDao.findByNewUserInfo(loginId,Name,signal,password2,birthDate);


//			if(i == 0) {
//			request.setAttribute("errMsg", "入力された内容は正しくありません");
//
//			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/new_user.jsp");
//			dispatcher1.forward(request, response);
//			return;
//		}

		response.sendRedirect("UserListServlet");

	}

}

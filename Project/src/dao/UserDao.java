package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao{
	public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;
	try{
		conn = DBManager.getConnection();

		String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, loginId);
        pStmt.setString(2, password);
        ResultSet rs = pStmt.executeQuery();

        if(!rs.next()) {
        	return null;
        }

        String loginIdData = rs.getString("login_id");
        String nameData = rs.getString("name");
        return new User(loginIdData, nameData);
	}catch(SQLException e){
		e.printStackTrace();
		return null;
	}finally{
		if(conn != null) {
			try {
				conn.close();
			}catch(SQLException e){
				e.printStackTrace();
				return null;
			}
		}
	 }
  }

	 public List<User> findAll() {

	        Connection conn = null;
	        List<User> userList = new ArrayList<User>();

	 try {
		 conn = DBManager.getConnection();
		 String sql = "SELECT * FROM user";

		 Statement stmt = conn.createStatement();
         ResultSet rs = stmt.executeQuery(sql);

         while(rs.next()) {
        	 int id = rs.getInt("id");
             String loginId = rs.getString("login_id");
             String name = rs.getString("name");
             Date birthDate = rs.getDate("birth_date");
             String password = rs.getString("password");
             String createDate = rs.getString("create_date");
             String updateDate = rs.getString("update_date");
             User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

             userList.add(user);
         }
	 }catch(SQLException e){
		 e.printStackTrace();
		 return null;
	 }
	 finally {
         if (conn != null) {
             try {
                 conn.close();
             } catch (SQLException e) {
                 e.printStackTrace();
                 return null;
             }
         }
     }

     return userList;

   }

	 public User findByuserDateInfo(String Id) {
	        Connection conn = null;
		try{
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, Id);
	        ResultSet rs = pStmt.executeQuery();

	        int id = rs.getInt("id");
	          String loginId = rs.getString("login_id");
	          String name = rs.getString("name");
	          Date birthDate = rs.getDate("birth_date");
	          String password = rs.getString("password");
	          String createDate = rs.getString("create_date");
	          String updateDate = rs.getString("update_date");

	        if(!rs.next()) {
	        	return null;
	        }

	        return new User(id, loginId, name, birthDate, password, createDate, updateDate);


		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}finally{
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e){
					e.printStackTrace();
					return null;
				}
			}
		 }
	  }

	 public List<User> userListfindAll(String loginId,String Name,String birthdate1,String birthdate2) {
		 Connection conn = null;
	        List<User> userlistDate = new ArrayList<User>();


		 try {
			 conn = DBManager.getConnection();

			 String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			 if(!loginId.equals("")) {
				 sql += " AND login_id = '" + loginId + "'";
			 }

			 if(!Name.equals("")) {
				 sql += " AND name LIKE '%" + Name + "%'";
			 }

			 if(!birthdate1.equals("")) {
				 sql += " AND birth_date >= '" + birthdate1 + "'";
			 }

			 if(!birthdate2.equals("")) {
				 sql += " AND birth_date <= '" + birthdate2 + "'";
			 }

			 Statement stmt = conn.createStatement();
	         ResultSet rs = stmt.executeQuery(sql);

	         while(rs.next()) {
			 int id = rs.getInt("id");
             String loginIdDate = rs.getString("login_id");
             String name = rs.getString("name");
             Date birthDate = rs.getDate("birth_date");
             String password = rs.getString("password");
             String createDate = rs.getString("create_date");
             String updateDate = rs.getString("update_date");
             User user = new User(id, loginIdDate, name, birthDate, password, createDate, updateDate);

             userlistDate.add(user);
               }

	         }catch(SQLException e){
	    		 e.printStackTrace();
	    		 return null;
	    	 }
	    	 finally {
	             if (conn != null) {
	                 try {
	                     conn.close();
	                 } catch (SQLException e) {
	                     e.printStackTrace();
	                     return null;
	                 }
	             }
	         }

	         return userlistDate;

	       }

			public int findByNewUserInfo(String loginId, String name,String password1,String password2,String birthdate) {
		        Connection conn = null;
		        int i=0;
			try{
				conn = DBManager.getConnection();

				String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now())";

				PreparedStatement pStmt = conn.prepareStatement(sql);
		        pStmt.setString(1, loginId);
		        pStmt.setString(2, name);
		        pStmt.setString(3, birthdate);
		        pStmt.setString(4, password1);
		         i=pStmt.executeUpdate();
		         System.out.println(i);


			}catch(SQLException e){
				e.printStackTrace();
			}finally{
				if(conn != null) {
					try {
						conn.close();
					}catch(SQLException e){
						e.printStackTrace();
					}
				}
			 }
			return i;
			}


				public User findBynewUserInfo(String loginId) {
			        Connection conn = null;
				try{
					conn = DBManager.getConnection();

					String sql = "SELECT * FROM user WHERE login_id = ?";

					PreparedStatement pStmt = conn.prepareStatement(sql);
			        pStmt.setString(1, loginId);
			        ResultSet rs = pStmt.executeQuery();

			        if(!rs.next()) {
			        	return null;
			        }

			        String loginIdData = rs.getString("login_id");
			        return new User(loginIdData);
				}catch(SQLException e){
					e.printStackTrace();
					return null;
				}finally{
					if(conn != null) {
						try {
							conn.close();
						}catch(SQLException e){
							e.printStackTrace();
							return null;
						}
					}
				 }
			  }

			 public User findByuserdateDateInfo(String Id) {
			        Connection conn = null;
				try{
					conn = DBManager.getConnection();

					String sql = "SELECT * FROM user WHERE id = ?";

					PreparedStatement pStmt = conn.prepareStatement(sql);
			        pStmt.setString(1, Id);
			        ResultSet rs = pStmt.executeQuery();

			        if(!rs.next()) {
			        	return null;
			        }


			        int id = rs.getInt("id");
			          String loginId = rs.getString("login_id");
			          String name = rs.getString("name");
			          Date birthDate = rs.getDate("birth_date");
			          String password = rs.getString("password");
			          String createDate = rs.getString("create_date");
			          String updateDate = rs.getString("update_date");


			        return new User(id, loginId, name, birthDate, password, createDate, updateDate);


				}catch(SQLException e){
					e.printStackTrace();
					return null;
				}finally{
					if(conn != null) {
						try {
							conn.close();
						}catch(SQLException e){
							e.printStackTrace();
							return null;
						}
					}
				 }
			  }

public int findByUpdateUserInfo(String name, String birthdate,String password1,String password2,String loginId) {
    Connection conn = null;
    int upuser=0;
try{
	conn = DBManager.getConnection();

	String sql = "UPDATE user SET name=?,birth_date=?,password=?,update_date=now() WHERE login_id=?";

	PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setString(1, name);
    pStmt.setString(2, birthdate);
    pStmt.setString(3, password1);
    pStmt.setString(4, loginId);
    upuser=pStmt.executeUpdate();
    System.out.println(upuser);


}catch(SQLException e){
	e.printStackTrace();
}finally{
	if(conn != null) {
		try {
			conn.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
 }
return upuser;
 }

public int findByUpdateInfo(String name, String birthdate,String loginId) {
    Connection conn = null;
    int upuserDate=0;
try{
	conn = DBManager.getConnection();

	String sql = "UPDATE user SET name=?,birth_date=?,update_date=now() WHERE login_id=?";

	PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setString(1, name);
    pStmt.setString(2, birthdate);
    pStmt.setString(3, loginId);
    upuserDate=pStmt.executeUpdate();
    System.out.println(upuserDate);


}catch(SQLException e){
	e.printStackTrace();
}finally{
	if(conn != null) {
		try {
			conn.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
 }
return upuserDate;
 }

public User findByUpuserDateInfo(String Id) {
    Connection conn = null;
try{
	conn = DBManager.getConnection();

	String sql = "SELECT * FROM user WHERE id = ?";

	PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setString(1, Id);
    ResultSet rs = pStmt.executeQuery();

    if(!rs.next()) {
    	return null;
    }


    int id = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      String createDate = rs.getString("create_date");
      String updateDate = rs.getString("update_date");


    return new User(id, loginId, name, birthDate, password, createDate, updateDate);


}catch(SQLException e){
	e.printStackTrace();
	return null;
}finally{
	if(conn != null) {
		try {
			conn.close();
		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}
	}
 }
}
public int findByUserDeleteInfo(String loginId) {
    Connection conn = null;
    int userDelete=0;
try{
	conn = DBManager.getConnection();

	String sql = "DELETE FROM user WHERE login_id=?";

	PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setString(1, loginId);
    userDelete=pStmt.executeUpdate();
    System.out.println(userDelete);


}catch(SQLException e){
	e.printStackTrace();
}finally{
	if(conn != null) {
		try {
			conn.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
 }
return userDelete;
 }

public String findBySignaluserInfo(String Password) {
	 Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";
		String result = null;

		try {
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(Password.getBytes(charset));
		result = DatatypeConverter.printHexBinary(bytes);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return result;
}

  }

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
　　<meta charset="UTF-8">
    <title>ログイン画面</title>
</head>
<body>
          <h1><center>ログイン画面</center></h1>
   <form class="form-signin" action="LoginServlet" method="post">
   <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		 <font color="red">${errMsg}</font>
		</div>
	</c:if>
  <p><center><label>ログインID　　　　<input type="text" name="id" size="30"></label></p>
　<p><label>パスワード　　　　<input type="password" name="pass" size="30"></label></p>
<br>
  <div class="row">
     <input type="submit"  value="ログイン" class="btn btn-primary">
      </center>
  </div>
  </form>
</body>
</html>
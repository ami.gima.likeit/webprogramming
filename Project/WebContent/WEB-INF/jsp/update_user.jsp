<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>

<title>ユーザ情報更新</title>

    </head>
<body>
    <div class="row">
        <div style="background-color:#808080;padding: 10px;border: 3px;">
        <div align="right">
        <font color="ffffff">ユーザ名さん　　　　</font>　
        <a href="LogoutServlet" class="navbar-link logout-link"><font color="red">ログアウト</font></a>
            </div></div>

    <h1><center>ユーザ情報更新</center></h1>

    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		 <font color="red">${errMsg}</font>
		</div>
	</c:if>
<form class="form-signin" action="UpdateUserSarvlet" method="post">
 <p>ログインID　　　　　　　　${userDate.loginId}</p>
 <input type="hidden" name="id" size="30" value="${userDate.loginId}">
 <div>パスワード　　　　　　　 <input type="password" name="pass1" size="30"></div><br>
<div>パスワード（確認）　　　　<input type="password" name="pass2" size="30"></div><br>
<div>ユーザ名　　　　　　　　　<input type="text" name="name" size="30" value="${userDate.name}"></div><br>
<div>生年月日　　　　　　　　　<input type="text" name="birthdate" size="30" value="${userDate.birthDate}"></div><br>
  <br>
  <div class="row">
      <center>
     <input type="submit" value="　　　更新　　　" class="btn btn-primary">
      </center>
  </div>
<br>
<a href="UserListServlet" class="navbar-link logout-link">戻る</a>

</body>
</html>

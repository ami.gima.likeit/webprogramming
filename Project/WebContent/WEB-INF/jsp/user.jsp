<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ一覧</title>

</head>
 <body>

    <div class="row">
        <div style="background-color:#808080;padding: 10px;border: 3px;">
        <div align="right">
        <font color="ffffff"><li class="navbar-text">${userInfo.name}さん</li></font>
        <a href="LogoutServlet" class="navbar-link logout-link"><font color="red">ログアウト</font></a>
            </div></div></div>

    <h1><center>ユーザ一覧</center></h1>

<form class="form-signin" action="UserListServlet" method="post">


    <p><div align="right"><a href="NewUserSarvlet" class="navbar-link logout-link">新規登録</a></div></p>

    <p><label>ログインID　　　　<input type="text" name="id" size="45"></label></p>
    <p><label>ユーザ名　　　　　   <input type="text" name="name" size="45"></label></p>

  <div class="row">
　  <p><label>
     生年月日　　　　　<input type="date" name="date1"  id="date" />　～　<input type="date" name="date2" id="date" />
    </label></p>


  <div align="right">
     <input type="submit" value="　　検索　　" class="btn btn-primary">
  </div>
  </div>
  </form>

<hr>
　<table border="1">
  <thead>
    <tr bgcolor="#dcdcdc">
      <th scope="col">　ログインID　</th>
      <th scope="col">　ユーザ名　</th>
      <th scope="col">　生年月日　</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  <c:forEach var="user" items="${userList}" >
    <tr>
      <td>${user.loginId}</td>
      <td>${user.name}</td>
      <td>${user.birthDate}</td>
       <td>
       <a class="btn btn-primary" href="UserServlet?id=${user.id}">詳細</a>
       <c:if test="${userInfo.loginId == 'admin' or userInfo.loginId == user.loginId}">
       <a class="btn btn-success" href="UpdateUserSarvlet?id=${user.id}">更新</a>
       </c:if>
       <c:if test="${userInfo.loginId == 'admin'}">
       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
       </c:if>
      </td>
    </tr>
    </c:forEach>
  </tbody>
  </table>

</body>
</html>

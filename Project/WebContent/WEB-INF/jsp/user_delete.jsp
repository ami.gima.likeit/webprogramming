<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>

<title>ユーザ削除確認</title>

</head>
<body>
    <div class="row">
        <div style="background-color:#808080;padding: 10px;border: 3px;">
        <div align="right">
        <font color="ffffff">ユーザ名さん　　　　</font>　
        <a href="log_in.html"><font color="red">ログアウト</font></a>
            </div></div>

    <h1><center>ユーザ削除確認</center></h1>

    <form class="form-signin" action="UserDeleteServlet" method="post">

<p>ログインID：${userDate.loginId}</p>
<input type="hidden" name="id" size="30" value="${userDate.loginId}">
<p>を本当に削除してよろしいでしょうか。</p>
<br>
<br>
<div class="row">
      <center>
     <input type="button" onclick="location.href='UserListServlet'" value="キャンセル" class="btn btn-primary">　　　　　
     <input type="submit" value="　ＯＫ　" class="btn btn-primary">
      </center>


</body>
</html>


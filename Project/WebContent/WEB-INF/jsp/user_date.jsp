<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>

<title>ユーザ情報詳細参照</title>

</head>
<body>
    <div class="row">
        <div style="background-color:#808080;padding: 10px;border: 3px;">
        <div align="right">
        <font color="ffffff">ユーザ名さん　　　　</font>　
        <a href="LogoutServlet" class="navbar-link logout-link"><font color="red">ログアウト</font></a>
            </div></div>

    <h1><center>ユーザ情報詳細参照</center></h1>

<form class="form-signin" action="UserServlet" method="post">
 <p>ログインID　　　　${userDate.loginId}</p>
 <p>ユーザ名　　　　　${userDate.name}</p>
 <p>生年月日　　　　　${userDate.birthDate}</p>
 <p>登録日時　　　　　${userDate.createDate}</p>
 <p>更新日時　　　　　${userDate.updateDate}</p>
<br>
<a href="UserListServlet" class="navbar-link logout-link">戻る</a>
</form>
</body>
</html>